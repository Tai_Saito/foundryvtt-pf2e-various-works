export const CONST = {
    name: "TayiWP",
    name_manifest: "tayiwp-pf2e",
    flagsdir: "tayiwp",
    DCS_BY_LEVEL: (lvl) => (lvl <= 20) ? (14 + lvl + Math.floor(lvl / 3)) : (lvl * 2)
};
