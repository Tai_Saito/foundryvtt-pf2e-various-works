import {CONST} from "./const.js";


export class TayiWP_DCWindow {
    static NAME = "dc-window";
    static last_used_level = 5;
}


export class TayiWP {
    static CONST = CONST;
    static DC_WINDOW = TayiWP_DCWindow;
}


Hooks.on("init", function () {
    globalThis.TayiWP = TayiWP
});


Hooks.on("renderChatLog", (chatTab, $html, user) => {
    if (!user.user.isGM) return;
    const $btn_div = $('<button>', {
        'id': `${CONST.flagsdir}_${TayiWP.DC_WINDOW.NAME}_btn-open`,
        'text': 'DC',
        'style': 'flex: 0; border-style: solid; border-color: dimgray; align-self: baseline; padding: 3px; ' +
                 'margin-left: 4px; line-height: normal;'
    });
    $btn_div.on('click', () => {
        new Application({
            width: 425,
            height: 425,
            popOut: true,
            minimizable: true,
            resizable: false,
            id: TayiWP.DC_WINDOW.NAME,
            template: `modules/${CONST.name_manifest}/application.html`,
            title: "DC Button generator",
        }).render(true);
    });
    const $label = $('#chat-controls label.chat-control-icon');
    $btn_div.insertAfter($label);
});
